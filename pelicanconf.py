#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Chris'
SITENAME = 'Twopercent Blog'
SITETITLE = 'Chris Moser'
SITESUBTITLE = '“Fear is the path to the dark side. Fear leads to anger. Anger leads to hate. Hate leads to suffering.” -Yoda'

PATH = 'content'
STATIC_PATHS = ['images','files']
SITELOGO = 'images/bitmoji.png'

TIMEZONE = 'America/Chicago'

DEFAULT_LANG = 'en'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('LibreMesh', 'https://libremesh.org/'),
         ('OpenWrt', 'https://openwrt.org/'),
         ('Positive Vector', 'https://linktr.ee/pvcv.run'),
         ('Broken Clock Brewing Cooperative', 'https://www.brokenclockbrew.com/'),)

# Social widget
# Names are 'Font Awesome' icon names (https://fontawesome.com/icons)
SOCIAL = (('linkedin', 'https://www.linkedin.com/in/chris-moser-93273843/'),
          ('gitlab', 'https://gitlab.com/twopercent'),
          ('mastodon', 'https://floss.social/@twopercent'),
          ('instagram', 'https://www.instagram.com/rollinwithchris/'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = "/home/chris/projects/pelican-themes/Flex"
