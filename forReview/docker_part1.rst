Boinc on Docker 
###############

:date: 2016-03-15 17:07
:tags: ops, CentOS, docker, BOINC
:summary: Installing docker and create a container for BOINC
:category: blog

`Docker <https://www.docker.com/>`_ (Build, Ship, Run) automates the deployment of applications inside software containers.
Virtual containers enable flexibility and portability, allowing applications to run on local or cloud infrastructure.

------------
Installation
------------

On CentOS 7:

.. code-block:: bash

    sudo yum -y install docker

Then test with:

.. code-block:: bash

    sudo docker run hello-world

This will perform a number of actions to test your installation.

.. code-block:: bash

    Unable to find image 'hello-world:latest' locally
    Trying to pull repository docker.io/library/hello-world ... latest: Pulling from library/hello-world
    b901d36b6f2f: Pull complete 
    0a6ba66e537a: Pull complete 
    Digest: sha256:8be990ef2aeb16dbcb9271ddfe2610fa6658d13f6dfb8bc72074cc1ca36966a7
    Status: Downloaded newer image for docker.io/hello-world:latest
    
    
    Hello from Docker.
    This message shows that your installation appears to be working correctly.
    
    To generate this message, Docker took the following steps:
     1. The Docker client contacted the Docker daemon.
     2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
     3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
     4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
     $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker Hub account:
     https://hub.docker.com

    For more examples and ideas, visit:
     https://docs.docker.com/userguide/

    Usage of loopback devices is strongly discouraged for production use. Either use `--storage-opt dm.thinpooldev` or use `--storage-opt dm.no_warn_on_loop_devices=true` to suppress this warning.

Continue with the `tutorial <https://docs.docker.com/mac/step_three/>`_ to grab a image from the `docker hub <https://hub.docker.com/>`_ and run it. 


--------------------------------
Build an Image from a Dockerfile
--------------------------------

Now lets look at the `httpd image <https://hub.docker.com/_/httpd/>`_ on docker hub.
Create a Dockerfile:

.. code-block:: bash
    
    FROM httpd:latest
    MAINTAINER "Chris Moser"
    
    COPY ./public-html/ /usr/local/apache2/htdocs/

Create a public-html directory with an index.html file inside:

.. code-block:: html
    
    <html>
      <head>
        <title>
          Docker Web Server
        </title>
      </head>
      <body>
        <h1>My Docker Web Server</h1>
        <p>This container is running the latest apache service</p>
      </body>
    </html>


Our Dockerfile instructs docker to use the latest version of the httpd image and add our website to the /usr/local/apache2/htdocs folder.
The httpd container has already been configured to run the apached web server when it starts, pointing to this directory as its document root.

Now build and start the container with:

.. code-block:: bash
   
    sudo docker build -t my-apache .
    sudo docker run -it --rm my-apache
   
Because we didn't setup a docker network bridge, we need to get the IP of the container on the docker virtual interface.
First run ps to get the container ID of our my-apache container and then use inspect to get the IP address.

.. code-block:: bash

    sudo docker ps
    sudo docker inspect <CONTAINER ID>

Point your local browser at this address and you should see your web page.

-----------------------
Inspect Existing Images
-----------------------

While none of the tools can explicitly produce a Dockerfile from any given image, they do well to give you the gist of how an image was created.
This is useful to get examples of how to create your own containers.

Use docker inspect to look at a report from the httpd base image and from our my-apache image.

.. code-block:: bash

    sudo docker inspect httpd
    sudo docker inspect my-apache

We can see some environment variables being set, the URL for the httpd package, and the httpd-foreground command.

For an even better view, try `imagelayers.io <https://imagelayers.io>`_. 
Put in the httpd image (it will grab it from docker hub) and it will display the likely Dockerfile instructions used to create it.
