TITLE: nanoblogger 
AUTHOR: Chris Moser
DATE: Thu Aug 25 17:30:08 CDT 2011
DESC: 
FORMAT: raw
-----
BODY:Install nanoblogger from your favorite package manager or extract the last archive from sourceforge and place it in your path. "nb --blog-dir /home/username/public_html/blog/ add weblog" will create all necessary files. Give it a fill path or it will try to write to /usr/share or something. If apache is running and configured correctly you should be able to browse ~username/blog and view the default layout. If you get apache errors check the permissions on files in the blog folder. You should run nb as your normal user account.

As the guide suggests, add the location of your new blog to nb.conf if you are writing just one blog.

Then run "nb add entry" It will fire up an editor and away you go. When you save and ext, it will add your entry to your blog.

I want the blog to be put on a public server so I added "rsync -av --delete /home/username/public_html/blog/ username@server:/home/username/public_html/blog/" to the blog.conf file so that I can rsync the blog directory to a web server with the "nb publish" command.

Cool.

EDIT: It will publish automatically after you exit if you have the BLOG_PUBLISH_CMD set.

END-----
