TITLE: CentOS 
AUTHOR: Chris Moser
DATE: Fri Jun  8 13:10:09 CDT 2012
DESC: 
FORMAT: 
-----
BODY:
I have been very happy with <a href="http://www.archlinux.org">Arch Linux</a> for the past few years. 
They have a great <a href="https://wiki.archlinux.org">Wiki</a>, allow you to use a variety of variety of desktop and window managers, and generally rank high at <a href="http://oswatershed.org">Open Source Watershed</a>. 
Now that I am getting more serious about RHCSA and RHCE certifications I decided I needed to switch to a Red Hat based OS. 
I used Fedora 16 for a project at work and it is quit nice but I am worried it is too far downstream from RHEL. 
I wouldn't want to get caught using a tool in Fedora that is not used in RHEL. 
I decided against purchasing a Red Hat software subscription and don't want updates to cease after 30 days so I settled on CentOS. 
So far it has been nice. 
KVM installed easily and Adding a few Windows VM's went smoothly. 
I installed nanoblogger manually (not in any of the mojor repos) and this will be my first test post. 
<br>
<br>
Here we go!
END-----
