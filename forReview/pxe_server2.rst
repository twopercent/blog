PXE Server Part 2 and Salt Stack
################################

:date: 2015-11-05 12:18
:tags: ops, saltstack, firewalld, kickstart
:summary: Initial setup of salt stack.
:category: blog

==================
Minions, Assemble!
==================

I built a few CentOS hosts with the PXE server I created in a `previous post <http://twopercent.github.io/pxe-server-part-1.html>`_.
I used a kickstart file to install 'minimal server' so the installations could be unattended.
After each host was installed, I manually set a hostname and static IP address.
This would not be feasible for a large deployment and there are a number of options available to set a unique hostname.

1. Kickstart can variables placed in the kernel options line of the syslinux installer. --> `monzell.com <http://monzell.com/post/15547967527/automatically-set-the-hostname-during-kickstart/>`_

2. Generate a randome hostname in the kickstart %pre section and then set it in the main section. --> `minuteware.net <http://minuteware.net/2012-10-18-random-hostname-for-centos-kickstart-installation.html/>`_

3. Modify your syslinux.cfg file between each build to pass a new hostname to the kickstart file via a kernel option.

4. Generate a unique kickstart file for each build.

As our end goal is wrangle these machines with `salt stack <http://saltstack.com/>`_ it is a good idea to have kickstart install the salt-minion client as well.
I used the bootstrap installer script by adding the following lines to the %post section of my kickstart file:

.. code-block:: bash

    curl -L https://bootstrap.saltstack.com -o install_salt.sh
    sudo sh install_salt.sh
    
You could also install the salt-minion package from EPEL.
Download the package to your PXE server, use createrepo to update your repository metadata, and add "salt-minion" to the %packages section of your kickstart file.
Alternatively you could add the EPEL repository and install the salt-minion package in the %post section of your kickstart file.

I don't have any DNS set up in my test environment so I added a line to the host file so the salt-minion client would find the master with the name "salt".
This is also in %post.

.. code-block:: bash

    echo "<IP_of_SaltMaster> salt" >> /etc/hosts

===============
The Salt Master
===============

I installed the salt master on my PXE server with the same bootstrap script by passing the -M flag.
`docs.saltstack.com <https://docs.saltstack.com/en/getstarted/fundamentals/install.html#>`_ details the installtion.

The salt minions will continuously try to reach the salt master so the order of events shouldn't matter here.
Check for incomming connections on the master with:

.. code-block:: bash

    salt-key --list-all

I found nothing and remembered the firewall was probably blocking the connections.
Add the file "salt-master.xml" to /etc/firewalld/services with the following content.

.. code-block:: bash

    <?xml version="1.0" encoding="utf-8"?>
    <service>
      <short>salt-master</short>
      <description>TCP port 4505 and 4506 for salt stack</description>
      <port protocol="tcp" port="4505"/>
      <port protocol="tcp" port="4506"/>
    </service>

Reload the firewall to find the new rule, add it, and reload again to activate it.

.. code-block:: bash


    sudo firewall-cmd --reload
    sudo firewall-cmd --permanent --add-service=salt-master
    sudo firewall-cmd --reload

If you still don't see minions with "salt-key --list-all" you can check the /var/log/salt/minion file on the minions for possible issues.
Finally, add the minions on the master and run a test command.

.. code-block:: bash

    salt-key --accept-all
    salt '*' test.ping
