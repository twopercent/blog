Salt Stack and BOINC
####################

:date: 2015-11-11 15:54
:tags: ops, saltstack, BOINC
:summary: Running BOINC with Salt Stack.
:category: blog

=============
Initial Setup
=============

Now that we have some hardware and Salt Stack running, lets create a state file and put the minions to work.
I am going to use `BOINC <http://boinc.berkeley.edu/>`_ to stress the CPU's.

The only boinc packages for CentOS 7 listed on `pkgs.org <http://pkgs.org/>`_ just moved out of epel-testing into epel after I completed my build.
They would be a good resource for a future configuration.

I downloaded the source and compiled boinc with the default options on my head node.
I attached to `World Community Grid <http://www.worldcommunitygrid.org/>`_ with the boinccmd tool supplying the project URL and my account key.

.. code-block:: bash

    boinccmd --project_attach www.worldcommunitygrid.org 122244_6d39631909f6dbc9491c34478ff24806

I don't intend to continue running BOINC on the salt master but wanted to make sure installaition worked and create an account file to supply to the minions.

==========
Salt State
==========

Now we should be able to create a salt state file, serve the boinc binaries and configuration files to the minions, and apply the state.
The salt master puts files in /srv/salt by default.
I created /src/salt/boinc to make things a little cleaner.
It includes:

.. code-block:: bash

    account_www.worldcommunitygrid.org.xml
    boinc
    boinc-client_init.d
    boinc-client_sysconfig

The xml file was created when I attached to a project on the salt master and when copied to anohter host will instruct BOINC to request work from the same project.
The boinc directory includes the compiled boinc application.
The boinc-client files are a startup script to be placed in /etc/init.d and a configuration file to be placed in /etc/sysconfig

In /srv/salt I also keep the boinc.sls state file:

.. code-block:: bash

    boinc:
      user.present:
        - home: /home/boinc

    /home/boinc/boinc/:
      file.recurse:
        - source: salt://boinc/boinc
        - user: boinc
        - group: boinc
        - clean: True

    /home/boinc/boinc/client/boinc_client:
        file.managed:
          - source: salt://boinc/boinc/client/boinc_client
          - user: boinc
          - group: boinc
          - mode: 775   

    /home/boinc/boinc/client/boinc:
      file.managed:
        - source: salt://boinc/boinc/client/boinc
        - user: boinc
        - group: boinc
        - mode: 775   

    /home/boinc/boinc/client/boinccmd:
      file.managed:
        - source: salt://boinc/boinc/client/boinccmd
        - user: boinc
        - group: boinc
        - mode: 775   

    /home/boinc/boinc/client/switcher:
      file.managed:
        - source: salt://boinc/boinc/client/switcher
        - user: boinc
        - group: boinc
        - mode: 775   

    /home/boinc/boinc/client/build_po:
      file.managed:
        - source: salt://boinc/boinc/client/build_po
        - user: boinc
        - group: boinc
        - mode: 775   
 
    /home/boinc/account_www.worldcommunitygrid.org.xml:
      file.managed:
        - source: salt://boinc/account_www.worldcommunitygrid.org.xml
        - user: boinc
        - group: boinc
        - mode: 664   

    /etc/init.d/boinc-client:
      file.managed:
        - source: salt://boinc/boinc-client_init.d
        - user: root
        - group: root
        - mode: 755

    /etc/sysconfig/boinc-client:
      file.managed:
        - source: salt://boinc/boinc-client_sysconfig
        - user: root
        - group: root
        - mode: 755

    start boinc:
      service.running:
        - name: boinc-client

The first statement tells salt to create a boinc user with the default home directory location.
Next we copy the boinc folder with the compiled boinc application to the boinc users newly created home directory.
Salt does not maintain the owner and permissions of files when copying with the "file.recurse" function so the next 5 statements verify these key binaries are executable.
We copy the account information file to the root of the boinc user home directory, the init script to /etc/init.d and the config file to /etc/sysconfig
Finally, the "start boinc" state verifes that the boinc service is running.

I want boinc to run on all my minions currently so we apply the state file with:

.. code-block:: bash

    salt '*' state.apply boinc

==========
Next Steps
==========

Now that a boinc-client package is available in EPEL, it would probably be cleaner to use that.
Another option would be to use the `BOINC github repository <https://github.com/BOINC/boinc>`_.
We could configure salt to pull, complile, and install the latest verion on each minion every time there is an update.


.. image:: http://www.worldcommunitygrid.org/getDynamicImage.do?memberName=c_moser&mnOn=false&stat=1&imageNum=1&rankOn=true&projectsOn=true&special=true
   :alt:

.. image:: wcg_stats.png
   :alt:
   
