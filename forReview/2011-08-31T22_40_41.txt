TITLE: Android Part 1 
AUTHOR: Chris Moser
DATE: Wed Aug 31 22:40:41 CDT 2011
DESC: 
FORMAT: 
-----

BODY:I'm creating an android application to execute simple queries about H-1B visa applications provided by the Foreign Labor Certification Datacenter database. 
American employers are required to apply for H-1B visas in order to hire non-immigrants for specialty positions in the U.S. 
This data is useful for salary seekers as it includes the wages offered to foreign workers. 
<a href="http://www.flcdatacenter.com">flcdatacenter.com</a> offers the date for free but the search feature is non-intuitive.
<br><br>
The actual android part of the system will be quite small. 
Just a couple fields to enter search criteria (location, title, salary, emplyer, etc.) and then a sceen to display the results. 
The meat of the project will be a LAMP stack, creating the queries in PHP and getting the data from FLCD into mySQL.
<br><br>
I decided to do the project on my laptop with the intention of hosting it somewhere else when finished. 
Once I've got all the pieces working together it should be straight forward to migrate it to a public location. 
I run ARCH linux and followed the ArchWIKI article on <a href="https://wiki.archlinux.org/index.php/LAMP">LAMP</a> to get everything installed. 
<br><br>
The next step is to get the data. 
It comes in a 50 MB zip file in either Microsoft Acess format or a comma deliniated text file. 
I chose the text file and it looks like I have to declare and define a  table with the proper data types in order to use the LOAD DATA INFILE command.
END-----
