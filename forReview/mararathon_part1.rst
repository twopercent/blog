Marathon Part 1
###############

:date: 2016-03-08 11:58
:tags: ops, mesos, marathon, zookeeper
:summary: Installaing Marathon with a single zookeeper master
:category: blog

------------
Introduction
------------

`Marathon <https://mesosphere.github.io/marathon/>`_ is "A cluster-wide init and control system for services in cgroups or Docker containers."
It is provided by `Mesosphere <https://mesosphere.com/>`_ and is a core component of their Data Center Operating System (DCOS).

We will need a running installation of mesos which we have `already installed <http://twopercent.github.io/apache-mesos-part-1.html>`_. 

---------
Zookeeper
---------

Zookeeper is a distributed coordination service from Apache.
A cluster or 'ensemble' of zookeeper nodes coordinate with each other through a shared hierarchical name space of data registers.
EPEL has no zookeeper package so download the tar file from `Apache Zookeeper <http://zookeeper.apache.org/>`_ site.
Alternatively, the mesosphere CentOS marathon package includes zookeeper.

We will confiure zookeeper in standalone mode in order to run Marathon.

Create a config file 'zoo.cfg' and place it in the conf directory.

.. code-block:: bash

   tickTime=2000
   dataDir=/var/zookeeper
   clientPort=2181

Allow access through the CentOS7 firewalld by creating zookeeper.xml and placing it in /etc/firewalld/services

.. code-block:: bash

   [cmoser@newton ~]$ ssh root@scfs "cat /etc/firewalld/services/zookeeper.xml"
   <?xml version="1.0" encoding="utf-8"?>
   <service>
     <short>zookeeper</short>
     <description>TCP port 2181 for zookeeper</description>
     <port protocol="tcp" port="2181"/>
   </service>

Add the service with firewall-cmd and reload the configuration.

In production we would configure 3 or 5 zookeeper nodes.
zoo.cfg would additionally list each node anlong with an ID number.
We would also need to specify two additional ports in zookeeper.xml.
2888 for peers to communicate with each other and 3888 for followers to connect to the leader.

Start zookeeper and check its status with:

.. code-block:: bash
   
   /bin/zkServer.sh start
   /bin/zkServer.sh status

--------
Marathon
--------

Download and unwind the `Marathon package <https://mesosphere.github.io/marathon/>`_

Start marathing referencing your localhost as the zookeeper node

.. code-block:: bash

   sudo ./bin/start --master zk://192.168.10.1:2181/mesos --zk zk://192.168.10.1:2181/marathon

You should be able to access the marathon UI at http://192.168.10.1:8080
