TITLE: firewalld
AUTHOR: Chris Moser
DATE: Tue Apr 28 16:05:20 CDT 2015
DESC: 
FORMAT: raw
-----
BODY:
Configuring a firewall with iptables can be a daunting task and often new administrators will revert to disabling it.
The firewalld service is an alternative to iptables service and allows for dynamic configuration.
We will look at a few firewall-cmd examples used to manipulate the firewalld service.
There is also a gui, firewall-config, but that is mostly self explanatory.
On a newly build CentOS system, iptables.service and iptables6.service will be disabled and firewalld.service will be enabled.
<br>
First check if the firewalld service is running.
<br><br>
<code>firewall-cmd --state</code>
<br><br>
Refer to the man page or use -h to see available options.
<br><br>
<code>firewall-cmd -h</code>
<br><br>
The two methods we will use to adjust the firewall are by modifying a port and modifying a service.
We will assume we are dealing with the currently default zone "public".
<br><br>
<b>Ports</b>
<br><br>
<code>firewall-cmd --list-ports</code>
<br><br>
This will show no ports have been manually adjusted.
<br><br>
Now suppose you want to open port 2049 for nfs.
<br><br>
<code>sudo firewall-cmd --permanent --zone=public --add-port=2049/tcp</code>  and  <code>sudo firewall-cmd --reload</code>
<br><br>
The zone declaration is optional and changes will be made to the default zone.
Without the <code>--permanent</code> flag rules will be added to the runtime firewall configuration but will not survive service resarts.
We reload the configuration for the change we made to take effect.
<br><br>
To reverse this change:
<br><br>
<code>sudo firewall-cmd --permanent --zone=public --remove-port=2049/tcp</code>  and  <code>sudo firewall-cmd --reload</code>
<br><br>
<b>Services</b>
<br><br>
Using services is very intuitive. First start by listing the services already defined by your firewalld package maintainers and the services already enabled.
<br><br>
<code>firewall-cmd --get-services</code>
<br>
<code>firewall-cmd --list-services</code>
<br><br>
Now we can see there is already a service defined for nfs.
Enable it with:
<br><br>
<code>sudo firewall-cmd --permanent --zone=public --add-service nfs</code>
<br>
<code>sudo firewall-cmd --reload</code>
<br><br>
Finally refer to <code>info firewalld</code> to see the locations of default/fallback icmptypes, services, and zones in <code>/usr/lib/firewalld</code> and system configuration settings in <code>/etc/firewalld</code>.
<br><br>
To define a new service, copy a sample .xml file from <code>/usr/lib/firewalld/services</code>  to  <code>/etc/firewalld/services</code> and modify it to your liking.
Then add it as a service as described above.
<br><br>
Check your work with <code>sudo iptables -L -n</code>
<br>
<br>
There are many other features and options in firewalld for you to explore.
END-----
