TITLE: X11 Forwarding 
AUTHOR: Chris Moser
DATE: Fri Jan  6 16:29:09 CST 2012
DESC: 
FORMAT: 
-----
BODY:
Sometime between Fedora 8 and Fedora 11, X11 forwarding was broken. 
<br><br>
This is what the users told me. 
<br><br>
A quick google search reveals "use ssh -X user@host" to resolve the symptom. 
The -X flag enables X11 forwarding through the secure shell connection. 
The problem (and I don't consider it a problem but some users, many of whom still use telnet and rlogin, consider it a problem) is a configuration setting in /usr/share/gdm/defaults.conf.
<br><br>
<code>"DissalowTCP=true"</code>
<br><br>
Setting this to "false" and resarting xorg-server allows users to forward X applications without the -X flag using xhost to specify which remote hosts have access. 
<br><br>
I installed KDE for user and he reported the same "problem" of X11 connections being refused. 
He scanned a number of machines in the office with nmap and noticed that some had TCP port 6000 open and others did not. 
<br><br>
<code>6000/tcp  open  X11</code>
<br><br>
The kdmrc file located at /etc/kde/kdm/kdmrc holds the TCP option for the KDE display manager. 
<br><br>
<code>ServerArgsLocal=-nr -nolisten tcp</code>
<br><br>
Removing the -nolisten tcp flag and restarting xorg-server will do the trick for KDE users.


END-----
