Title: Advent of Code 2015
Date: 2015-12-10 16:10
Category: blog
Tags: dev, python
Summary: The arrival of a notable person, thing, or event

I came across the [Advent of Code](http://adventofcode.com/) website after I saw someone create a repository on github to store their solutions.
It was created by [Eric Wastl](http://was.tl/) and allows you to complete programming challenges each day in whatever language you choose.
This is a nice approach as you don't have to worry about an online interpreter missing some modules that you have locally.

I have put my solutions so far on [github](https://github.com/twopercent/advent_2015)

