IRQ 70: nobody cared
####################

:date: 2015-10-15 15:15
:tags: ops, centos, ROCKS
:summary: Intel 82574L adapter sending IRQ's not handled by e1000e module
:category: blog

Nagios reported one of our `Rocks <http://www.rocksclusters.org/>`_ clusters lost all of its compute nodes.
A Rocks head node typically has two or more network interfaces.
One for public access to the cluster and another for a private network of compute nodes.
Since I had a local technician on chat, I asked him to check the switch of the cluster network.
It is a un-managed switch and a failure would cause this sympton.
He reported it looked operational so I checked the logs and found this:

.. code-block:: bash
    
    tesla kernel: irq 70: nobody cared (try booting with the "irqpoll" option)
    tesla kernel: Pid: 0, comm: swapper Tainted: G        W  ---------------    2.6.32-504.16.2.el6.x86_64 #1
    tesla kernel: Call Trace:
    tesla kernel: <IRQ>  [<ffffffff810eccab>] ? __report_bad_irq+0x2b/0xa0
    tesla kernel: [<ffffffff810eceac>] ? note_interrupt+0x18c/0x1d0
    tesla kernel: [<ffffffff810ed4f5>] ? handle_edge_irq+0xf5/0x180
    tesla kernel: [<ffffffff8100fbd9>] ? handle_irq+0x49/0xa0
    tesla kernel: [<ffffffff81533f9c>] ? do_IRQ+0x6c/0xf0
    tesla kernel: [<ffffffff8100b9d3>] ? ret_from_intr+0x0/0x11
    tesla kernel: <EOI>  [<ffffffff81425f31>] ? poll_idle+0x41/0x80
    tesla kernel: [<ffffffff81425f03>] ? poll_idle+0x13/0x80
    tesla kernel: [<ffffffff81426117>] ? cpuidle_idle_call+0xa7/0x140
    tesla kernel: [<ffffffff81009fc6>] ? cpu_idle+0xb6/0x110
    tesla kernel: [<ffffffff8152328d>] ? start_secondary+0x2be/0x301
    tesla kernel: handlers:
    tesla kernel: [<ffffffffa0164fe0>] (e1000_msix_other+0x0/0x130 [e1000e])
    tesla kernel: Disabling IRQ #70

/proc/interrupts and lspci confirmed irq 70 was addigned to one of the Intel 82574L Gigabit Ethernet adapters and the e1000e kernel module was in place to control this hardware.
The log messages seem to indicate the the network adapter was sending an interrupt request that the e1000e module was not handling.
The kernel decided this is not good and disabled the irq channel and none of properly formed requests are handled.
Restarting the network service resolved the symptom.

.. code-block:: bash

    sudo service network restart

After the issue arose again, I determined we needed to look for the cause.
This `bug report <http://ehc.ac/p/e1000/bugs/360/>`_ seemed relevant.
It appears there may be a hardware issue with this particular adapter.
I decided to attempt the module option declaration suggested.

I created the /etc/modprobe.d/e1000e.conf file witht the following line:

.. code-block:: bash

    options e1000e IntMode=1,1,1

Three 1's because I have three adapters.
The module options are only read when the module is loaded and I have to bring down the network to reload the module.
I have IPMI baseboard management access to do this.
Stopping the network service while using secure shell, remote shell, telnet, or and other remote tool will terminate your connection and you will need console access to continue.

.. code-block:: bash

    sudo service network stop
    sudo rmmod e1000e
    sudo modprobe e1000e
    sudo service network start

A note in /var/log/messages tells me the option was parsed:

.. code-block:: bash

    tesla kernel: e1000e 0000:04:00.0: Interrupt Mode set to 1

It has been a week and there have been no more errors.
