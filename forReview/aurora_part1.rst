Apache Aurora Tutorial
######################

:date: 2016-03-03 15:28
:tags: ops, debian, mesos, aurora, vagrant, virtualbox
:summary: The Aurora 0.11.0 tutorial
:category: blog

------------
Introduction
------------

`Apache Aurora <http://aurora.apache.org/>`_ is a Mesos framework for long-running services and cron jobs.
`Marathon <https://mesosphere.github.io/marathon/>`_ from `Mesosphere <https://mesosphere.com/>`_ provides similar functionality.

While Aurora is a framework for mesos, which we have `already installed <http://twopercent.github.io/apache-mesos-part-1.html>`_, this tutorial uses vagrant to build a single VM running mesos master, zookeeper, aurora, and mesos slave. 

-------
Vagrant
-------

Grab a package from your repository or `Vagrant <https://www.vagrantup.com/>`_'s `downloads page <https://www.vagrantup.com/downloads.html/>`_.

I prefer KVM over virtual box so I intalled the `vagrant-libvirt <https://github.com/pradels/vagrant-libvirt>`_ plugin.
There turned out to be some features no yet implemented in the libvirt plugin that the aurora vagrant file uses.
So I retreated to virtual box.

-----------
Virtual Box
-----------

Since VirtualBox contains a non-free compiler, debian has moved its packages to contrib.
Add "contrib" to your /etc/apt/sources.list file, update, and install.

You can also get a debian package from `Virtual Box <http://www.virtualbox.org>`_'s `downloads page. <https://www.virtualbox.org/wiki/Linux_Downloads/>`_

------
Aurora
------

Clone the aurora git repository with:

.. code-block:: bash

   git clone git://git.apacke.org/aurora.git


Enter the aurora directory and run vagrant up.
Vagrant will read the 'Vagrantfile' which describes what type of VM to provision, set some network and memory variables and execute the examples/vagrant/provision-dev-cluster.sh script.
The script will install all the necessary prerequisites and then mesos and aurora.

I ran into an issue with the provision-dev-cluster.sh script.
A recent change in http://people.apache.org/ caused a broken link to the thrift compiler binary.

.. code-block:: bash

   ==> devcluster: + thrift_deb=thrift-compiler_0.9.1_amd64.deb
   ==> devcluster: + wget -c http://people.apache.org/~jfarrell/thrift/0.9.1/contrib/deb/ubuntu/12.04/thrift-compiler_0.9.1_amd64.deb
   ==> devcluster: --2016-03-04 21:57:10--  http://people.apache.org/~jfarrell/thrift/0.9.1/contrib/deb/ubuntu/12.04/thrift-compiler_0.9.1_amd64.deb
   ==> devcluster: Resolving people.apache.org (people.apache.org)... 
   ==> devcluster: 163.172.16.173
   ==> devcluster: , 
   ==> devcluster: 163.172.16.173
   ==> devcluster: Connecting to people.apache.org (people.apache.org)|163.172.16.173|:80... 
   ==> devcluster: connected.
   ==> devcluster: HTTP request sent, awaiting response... 
   ==> devcluster: 404 Not Found
   ==> devcluster: 2016-03-04 21:57:10 ERROR 404: Not Found.
   The SSH command responded with a non-zero exit status. Vagrant

I checked the `JIRA aurora <http://issues.apache.org/jira/browse/AURORA/>`_ page and didn't see anyting related.
I hopped into #aurora on irc.freenode.net and asked if this was a known issue.

I prepared a patch for aurora's github respository but it is a mirror of the apache repo.
You are asked to submit updates via their review board tool.
Since I didn't have a ReviewBoard account `zmanji <https://reviews.apache.org/users/zmanji/>`_ created a `review <https://reviews.apache.org/r/44347/diff/1#index_header/>`_ for me.

After this change in the provision-dev-cluster.sh script, vagrant up finished successfully.


---------
Run a Job
---------

Copy the provided python program and aurora configuration file.
Verify the /etc/aurora/cluster.json file is accurate.
Run the job with:

.. code-block:: bash

   aurora job create devcluster/www-data/devel/hello_world /vagrant/hello_world.aurora

If all goes well, you should see the job being scheduled and run on the mesos and aurora status web pages.

Kill your job and destroy your cluster when finished.
