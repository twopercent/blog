Apache Mesos Part 1
###################

:date: 2016-2-23 15:12
:tags: ops, CentOS, mesos
:summary: Installing Apache Mesos on CentOS 7.2
:category: blog

------------
Installation
------------

`Apache Mesos <http://mesos.apache.org/>`_ is an open-source cluster manager.
It "provides efficient resource isolation and sharing across distributed applications, or frameworks"

I have a fairly vanilla CentOS 7.2 host to use for this installation as well as some CentOS 7.2 slave hosts.
First make sure it is up to date.

.. code-block:: bash

   sudo yum -y update

The installation is straight forward following the `Getting Started <http://mesos.apache.org/gettingstarted/>`_ guide.

Pull down the latest mesos.*.tar.gz, verify all the dependencies are installed, and start the compilation.
I followed the same procedure for a slave host.

I copied the entire mesos-0.27.1 directory to a second slave host.
After performing all the prerequisites, I was able to 'sudo make install' without re-compiling.

--------
Firewall
--------

Configure firewalld with firewall-cmd on the master and slave nodes to allow access to port 5050.
Create a 'mesos.xml' file in /etc/firewalld/services

.. code-block:: bash

   <?xml version="1.0" encoding="utf-8"?>
   <service>
     <short>mesos</short>
     <description>TCP port 5050 for mesos</description>
     <port protocol="tcp" port="5050"/>
   </service>

Add the service and reload the settings

.. code-block:: bash

   sudo firewall-cmd --permanent --add-service=mesos
   sudo firewall-cmd --reload

Check your work with:

.. code-block:: bash

   sudo iptables -nL

Firewall settings should be handled by your configuration management solution of choice or via your bare-metal provisioning system.
Perhaps that will be a later project. 

-------
Startup
-------

From the build directory start the master with:

.. code-block:: bash

   sudo ./bin/mesos-master.sh --ip=192.168.10.1 --work_dir=/var/lib/mesos --cluster=MotionLogic

On each slave, run:

.. code-block:: bash

   sudo ./bin/mesos-slave.sh --master=192.168.10.1

On the master we can open the web interface at http://192.168.10.1:5050 and see that two slaves have connected.
Plenty of output is provided on the terminal to troubleshoot any issues.

------
Update
------

In researching `Apache Aurora <https://aurora.apache.org/>`_ I came across CentOS 7 rpms for current Mesos versions provided by `Mesosphere <https://mesosphere.com/>`_.

http://aurora.apache.org/documentation/latest/installing/#mesos-on-centos-7

.. code-block:: bash

   sudo rpm -Uvh http://repos.mesosphere.io/el/7/noarch/RPMS/mesosphere-el-repo-7-1.noarch.rpm
   sudo yum repository-packages mesosphere list



