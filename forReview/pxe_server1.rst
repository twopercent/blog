PXE Server Part 1
#################

:date: 2015-10-09 13:00
:tags: ops, centos, fierwalld, kickstart
:summary: Initial setup of PXE server, dhcp, tftp, http to provision hardware
:category: blog

Let's set up a test environment for development and operations related projects.
I have a number of rack mounted machines at my disposal so I will set up a PXE boot enviroment to provision my physical hosts.
I will start with a wiki article from `CentOS HowTos <https://wiki.centos.org/HowTos/PXE/PXE_Setup/>`_.
You can tell if is from an older verision of CentOS because of CentOS 3, 4, and 5 images they use.

I started with minimal install of CentOS 7. I installed and configured tftp-server and syslinux as described.
/usr/lib/syslinux does not exist and must have been moved since this how-to was created.
We can use rpm to find the syslinux files we are looking for:

.. code-block:: bash
    
    rpm -ql syslinux |grep pxelinx.0

/tftpboot also does not exist.

.. code-block:: bash
   
    rpm -ql tftp-server |grep tftpboot

I copied the required files from /usr/share/syslinux to /var/lib/tftpboot.
The kernel and initial ramdisk images needed can be found after the next step.
The how-to gives different locations for the syslinux images in your tftpboot tree and PXE Menu file, make sure yours are the same.
My pxelinux.cfg/default file looks like this:

.. code-block:: bash
   
    default menu.c32
    prompt 0
    timeout 300
    ONTIMEOUT local

    MENU TITLE Cluster Lab PXE Menu

    LABEL CentOS 7 Interactive
        MENU LABEL CentOS 7 Interactive
        KERNEL images/centos/7/x86_64/pxeboot/vmlinuz
        APPEND initrd=images/centos/7/x86_64/pxeboot/initrd.img inst.repo=http://192.168.10.1/centos/7/os/x86_64

    LABEL CentOS 7 KS Minimal
        MENU LABEL CentOS 7 KS Minimal
        KERNEL images/centos/7/x86_64/pxeboot/vmlinuz
        APPEND initrd=images/centos/7/x86_64/pxeboot/initrd.img inst.repo=http://192.168.10.1/centos/7/os/x86_64 ks=http://192.168.10.1/kickstart/centos7_minimal.ks

Instead of using vsftpd I went with httpd.
I copied the CentOS 7 DVD to the /var/www/html/centos/7/os/x86_64 directory.

The dhcpd.conf file seems to be dated as well.
Take a look at the example file located at /usr/share/doc/dhcp*/dhcpd.conf.example.
I added next-server and filename declarations from the how-to that aren't specified in the example file.
Use sudo journalctl -xn 30 if the dhcp daemon fails to start.
If you don't specity 30 lines, 10 will be given and the useful information will be missing.


I estimate 50% of linux tutorials cop out with a statement like "Disable the firewall and SELinux" at the start.
This is unfortunate, but understandable as iptables takes some getting used to.
Things are a little easier in CentOS 7 with firewalld.

.. code-block:: bash

    firewall-cmd --get-services
    sudo firewall-cmd --permanent --add-service=dhcp --add-service=ftp --add-service=http
    sudo firewall-cmd --reload

Check your work with:

.. code-block:: bash

   firewall-cmd --list-services

Now boot one of your client machines and select PXE boot. If all went well you should be greeted with the PXE menu you created.
Depending on if you confiugred a kickstart file in your PXE menu or opted for a manual install, proceed with installation.
