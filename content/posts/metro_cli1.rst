Metro Transit CLI Bus Locations
###############################

:date: 2015-09-22 16:37
:tags: python, REST, dev
:category: blog
:summary: Start of one of my early python projects to interact with the Metro Transit API

`Metro Transit <http://www.metrotransit.org>`_ is the main transit operator in the twin cities. 
They have a fine website but a little too much clicking is required to find out when the next bus is going to roll by. 
I found the `Metro Transit Web Services <http://svc.metrotransit.org/>`_ page to be just what I needed. 

I wrote three functions to request the applicable directions of a specific route, the time stops on a route, and the next arrivals at a time stop. 
There is also some logic to present the user with the results of each query and enter input to craft the next request. 
I used

.. code-block:: bash
    
   urllib.request</code>
   
and

.. code-block:: bash
    
   xml.etree.ElementTree
   
from the python 3.4 standard library. 

First the user is prompted for a route number and a call is made to the API to return the directions, either North - South or East - West, of that route. 
The data is returned in XML format as an array of text value pairs, the direction and a direcion code.
ElementTree is used to parse them. 
I load the data into a list of lists so that I can display the options with a <code>0.</code> and <code>1.</code> identifier. 
The users input is used to select the index of the parent list, taking the first index of the child list, which is the direction code. 

The direction code is used alond with the bus number to craft the next API call.  
An array of pairs is again returned. 
Time stops and a four character stop code are returned in order of the direction that was specified. 
A list of lists is again created and the users is promtped for a seletion. 

The final call combines the route number, the direction of travel and the time stop location. 
A collection of "NexTripDeparture" are returned which contain information about the next vehicles scheduled to stop at the requested location. 
I print the arrival time, bus number, route description, and a "BlockNumber" designator for the vehicle. (maybe a bus number) 

The code is on `GitHub <https://github.com/twopercent/mtcli>`_  

Use git clone to clone is locally: 

.. code-block:: bash

   git clone https://github.com/twopercent/mtcli

Some more work can be done to pull out the XML parsing from the API call functions as it is very simiar in all three. 
