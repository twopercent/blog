Title: RAID 1 with LVM
Date: 2024-12-18
Category: posts 
Tags: raid, lvm
Slug: lvm-raid
Author: Chris

## Convert an existing linear logical volume to a raid1 or mirror 

### Backgrond

I have a server running on a single disk. I now find myself relying on it more and want redundant storage.
The server installer created a single volume group on a single physical volume.

I have added a second disk to the system. I will duplicate the paritioning of the original drive, create a physical volume on it, and convert the existing volume group into a raid1 or mirror across both physical volumes.

## Tools
```
sudo apt install lvm2
```

Examine disks:
```
lsblk
```

Examine physical volumes:
```
pvscan
pvs
pvdisplay
```

Examine volume groups:
```
vgscan
vgs
vgdisplay
```

Examine logical volumes:
```
lvscan
lvs
lvdisplay
```

### Prepare the new disk

The new disk is identical in size to the original. Use `sfdisk` to dump the partition table from the original disk and apply it to the new one. THIS WILL DESTROY ALL THE DATA ON /dev/sdb.
```
sudo sfdisk --dump /dev/sda > part_table
sudo sfdisk /dev/sdb < part_table
```

I have two additional partitions (EFI and boot) that I will attempt to manually sync at a later time. For now, create a new lvm physical volume on the new disk.
```
sudo pvcreate /dev/sdb3
```
And add it to the existing volume group.
```
sudo vgextend <vg_name> /dev/sdb3
```

And finally:
```
lvconvert --type raid1 --mirrors 1 <vg_name>/<lv_name>
```
