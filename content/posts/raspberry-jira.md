Title: Raspberry Jira
Date: 2024-02-09
Category: posts 
Tags: rpi, hardware
Slug: raspberry-jira
Author: Chris

## Raspberry Pi with an e-ink display to show the status of a Jira board

### Parts

- [Raspberry Pi 3 Model B+](https://www.raspberrypi.com/products/raspberry-pi-3-model-b-plus/)
- [Waveshare 2.7inch e-Paper HAT](https://www.waveshare.com/product/displays/e-paper/epaper-2/2.7inch-e-paper-hat.htm)

### Setup

I used [rpi-imager](https://www.raspberrypi.com/software/) to flash Raspberry Pi OS Lite (64-bit) onto a 4GB SD card.
Since we don't require a desktop, we can use the smaller "Lite" image which will fit onto a 4GB card.

Configure a user name and password on first boot. Then set localisation and network settings with `sudo raspi-config` You can get the latest packages with `raspi-config` as well

Follow the [Waveshare wiki](https://www.waveshare.com/wiki/2.7inch_e-Paper_HAT_Manual#Enable_SPI_Interface) to enable the SPI interface:

Use `raspi-config` tool. SPI is enabled in `Interfacing Options` -> `SPI`

Skip the `C` section in the wiki and proceed with the `Python` installation.
Skip python2 packages (we'll use python3) as well as `pip`. 
All of the python requirements can be installed with the Debian package manager `apt`.
In Debian Bookworm and later, the python3 interpreter is marked `externally-managed`.
This means `pip` will not work unless extra steps are taken.
Read more about it [here](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.en.html#python3-pep-668)


```
sudo apt update
sudo apt install git python3-pil python3-numpy python3-rpi.gpio python3-spidev python3-dotenv
```

Review the demo code from Waveshare or 



