Title: Pandoc 3 breaks Pandoc Resume
Date: 2023-12-26
Category:
Tags: debian, apt
Slug: pandoc3-braeks-pandoc-resume
Author: Chris


I've been using Mark Szepieniec's [Markdown Resume](http://mszep.github.io/pandoc_resume/) but recently noticed a degradation in the styling. 

<!---
It might look better to use HTML for the columns, I didn't try it
-->
|<a href="files/johnnycoder.pdf">Original</a> version of the resume:|<a href="files/johnnycoder_broken.pdf">Current</a> version of the resume:|
|---|---|
|<object data="files/johnnycoder.pdf" type="application/pdf" height=300px></object>|<object data="files/johnnycoder_broken.pdf" type="application/pdf" height=300px></object>|

The tool relies on pandoc and context

`pandoc --version` and `context --version` reveal:

```
pandoc 3.0.1
mtx-context     | ConTeXt Process Management 1.04
```

Pandoc was just updated to major release 3 in [Debian Trixie](https://packages.debian.org/search?keywords=pandoc&searchon=names&exact=1&suite=all&section=all) (testing) and could be causing the issue.

This [issue](https://github.com/mszep/pandoc_resume/issues/94) in the [pandoc_resume](https://github.com/mszep/pandoc_resume) github repo also suggests updated pandoc is the cause.

Use `sudo apt-cache policy pandoc` to see if a previous version is still available. 

None were so I added (uncommented) the stable release in my /etc/apt/sources.list file.

Then `sudo apt update`, check the policy again and I can see pandoc `2.17.1.1-2~deb12u1` is available.

Remove pandoc 3 as well as the pandoc-data depencancy:

```
sudo apt remove pandoc
sudo apt autoremove
```

Finally install pandoc 2:
```
sudo apt install pandoc=2.17.1.1-2~deb12u1 pandoc-data=2.17.1.1-2~deb12u1
```
Hold the pandoc 2 package:
```
sudo apt-mark hold pandoc
```
Alternatively, use the included docker-compose.yml file to build your resume with a docker container that contains a pandoc 2
```
docker-compose up -d
```
