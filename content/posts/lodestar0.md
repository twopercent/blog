Title: Lodestar on Ubuntu
Date: 2022-02-10
Modified: 2022-02-11
Category: crypto
Tags: crypto, Typescript
Slug: lodestar-on-ubuntu-pt0
Author: Chris

In order to create a robust decentralized blockchain, Ethereum 2 will require a diverse set of clients. Prysm and Lighthouse are currently dominating. I will install `Lodestar`, a Eth2 implementation written in Typescript maintained by ChainSafe Systems via npm  

---

[Lodestar Documentation](https://chainsafe.github.io/lodestar/)


### Install Ubuntu

Install the latest Ubuntu LTS release (currently [Ubuntu 20.04.3 LTS
Focal Fossa](https://ubuntu.com/download/server))

Ubuntu desktop and server are the same but offer different default packages. I started with server because we don't need a desktop environment, office suite, web browser...

We will need `make`. This is achieved with:

```sudo apt install build-essential```

### Lodestar requirements

The `nodejs` package in Ubuntu repositories is crusty. Install nvm (node version manager) Check their [Github page](https://github.com/nvm-sh/nvm#installing-and-updating) for the latest version of the install script. Update your environment variables after running the script.

```curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash```

```source .bashrc```

Now install node Long Term Support and yarn

```nvm install --lts```

```nvm use --lts```

```npm install -g yarn```

### Install lodestar

```npm install -g @chainsafe/lodestar-cli```

### Run

```lodestar --help```

If you get a `-bash: /home/USER/.nvm/versions/node/v16.14.0/bin/lodestar: Permission denied` error, run:

```chmod +x ~/.nvm/versions/node/v16.14.0/lib/node_modules/@chainsafe/lodestar-cli/lib/index.js```
