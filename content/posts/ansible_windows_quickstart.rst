Ansible for Windows
###################

:date: 2016-05-31 16:24
:tags: ops, CentOS, Ansible
:summary: Install Anbiel on CentOS 7 and ping some Windows hosts
:category: blog

A co-worker of mine is using `Ansible <https://www.ansible.com/>`_ to manage some windows hosts. 
I set up my own environment to do some testing for him. 
Recently released Ansible 2.1 has some new features for Windows management so I installed this from epel-testing. 
sshpass is also required. 

------------
Installation
------------

On CentOS 7: 

.. code-block:: bash

    sudo yum -y --enablerepo epel --enablerepo epel-testing install ansible sshpass

You will also need pywinrm as well: 

.. code-block:: bash

   sudo pip install pywinrm

-------------------------
Configure Windows Clients
-------------------------

I followed the `Windows System Prep <http://docs.ansible.com/ansible/intro_windows.html#windows-system-prep/>`_ guide on `docs.ansible.com <http://docs.ansible.com/>`_. 
The first suggestion is to run a powershell script on each Windows host. 
I have no reason to believe Ansible would be mailicious but I don't like the idea of running this script without needing to. 
Getting started, we only need to verify that powershell 3.0.0 or later is installed and that the WinRM service is installed and running. 
Open powershell and run the following commands: 

.. code-block:: powershell

   $PSVersionTable.PSVersion
   Get-Service "WinRM"

If powershell is too old or the WinRM service is not installed or not running, remedy these before moving on. 

Create a local administrator account for Ansible to use. 

-----------------
Configure Ansible
-----------------

Change to /etc/ansible and modify the "hosts" file. 
Declare a [windows] group and list one or more Windows hosts. 

.. code-block:: bash

   [windows]
   workPC.mydomain.com
   homePC.mydonain.com

Create the /etc/ansible/group_vars directory and create a "windows.yml" file in it. 
This will describe the local account to use, password, and connection settings. 

.. code-block:: yaml

   ansible_user: ansible
   ansible_password: secret_password
   ansible_port: 5985
   ansible_connection: winrm

Port 5985 is the default http port for WS-Management. 
We can switch to 5986 after an SSL certificate is arranged and installed. 

-----------
The Results
-----------

That should be all we need. 
On the control server issue the following command: 

.. code-block:: bash

   ansible windows -m win_ping


.. code-block:: bash

   workPC.mydomain.com | SUCCESS => {
       "changed": false, 
       "ping": "pong"
   }
   homePC.mydomain.com | SUCCESS => {
       "changed": false, 
       "ping": "pong"
   }
